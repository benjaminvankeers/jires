package com.ineat.jires;

import java.net.URI;
import java.net.URISyntaxException;

import javax.ws.rs.core.MultivaluedMap;

import com.atlassian.jira.rest.client.JiraRestClient;
import com.atlassian.jira.rest.client.JiraRestClientFactory;
import com.atlassian.jira.rest.client.domain.Issue;
import com.atlassian.jira.rest.client.internal.async.AsynchronousJiraRestClientFactory;
import com.atlassian.util.concurrent.Promise;
import com.ineat.jires.bean.jira.AuthorBean;
import com.ineat.jires.bean.jira.WorklogBean;
import com.ineat.jires.bean.tempo.AssigneeBean;
import com.ineat.jires.bean.tempo.PlanBean;
import com.ineat.jires.bean.tempo.PlanItemBean;
import com.ineat.jires.bean.tempo.RecurrenceBean;
import com.ineat.jires.bean.tempo.ScopeBean;
import com.sun.jersey.api.client.Client;
import com.sun.jersey.api.client.WebResource;
import com.sun.jersey.api.client.filter.HTTPBasicAuthFilter;
import com.sun.jersey.core.util.MultivaluedMapImpl;

/**
 * Hello world!
 *
 */
public class App 
{
    public static void main( String[] args ) throws URISyntaxException
    {
    	 /*final JiraRestClientFactory factory = new AsynchronousJiraRestClientFactory();
         final URI jiraServerUri = new URI("https://dev-jira.ineat-conseil.fr");
         final JiraRestClient restClient = factory.createWithBasicHttpAuthentication(jiraServerUri, "benjaminvan", "Ineat59");
         
         final Promise<Issue> issue = restClient.getIssueClient().getIssue("TEST-46");
         
         System.out.println(issue);
*/
    	
    	//addTempoPlan();
    	addWorklog();
    	
    	
    }
    
    private static void addTempoPlan() {
    	
    	PlanBean plan = new PlanBean();
    	
    	AssigneeBean assignee = new AssigneeBean();
    	assignee.setKey("benjaminvan");
    	assignee.setType("user");
    	
    	PlanItemBean planItem = new PlanItemBean();
    	planItem.setId(new Long(10203));
    	planItem.setKey("TEST-46");
    	planItem.setType("ISSUE");
    	
    	RecurrenceBean recurrence = new RecurrenceBean("NEVER");
    	ScopeBean scope = new ScopeBean("team"); 
    	
    	plan.setAssignee(assignee);
    	plan.setPlanItem(planItem);
    	plan.setRecurrence(recurrence);
    	plan.setScope(scope);
    	plan.setCommitment(25);
    	plan.setStart("2019-03-13");
    	plan.setEnd("2019-03-13");
    	
    	TempoClient tempoClient = new TempoClient();
    	tempoClient.postPlan(plan);
    }
    
    private static void addWorklog() {
    	
    	WorklogBean worklog = new WorklogBean();
    	AuthorBean author = new AuthorBean();
    	
    	author.setName("benjaminvan");
    	worklog.setAuthor(author);
    	worklog.setComment("test api java");
    	worklog.setStarted("2019-03-14T09:00:00.000+0100");
    	worklog.setTimeSpentSeconds(7200);
    	
    	JiraClient jiraClient = new JiraClient();
    	jiraClient.postWorkLog(worklog, "TEST-46");
    }
}
