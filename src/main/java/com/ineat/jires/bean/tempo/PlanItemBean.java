package com.ineat.jires.bean.tempo;

import javax.xml.bind.annotation.XmlRootElement;

@XmlRootElement(name = "planItem")
public class PlanItemBean {
	
	
	private String type;
	private Long id;
	private String key;
	
	public PlanItemBean() {
		
	}
	
	public String getType() {
		return type;
	}
	public void setType(String type) {
		this.type = type;
	}
	public Long getId() {
		return id;
	}
	public void setId(Long id) {
		this.id = id;
	}
	public String getKey() {
		return key;
	}
	public void setKey(String key) {
		this.key = key;
	}
	
	

}
