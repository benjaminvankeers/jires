package com.ineat.jires.bean.tempo;

import javax.xml.bind.annotation.XmlRootElement;

@XmlRootElement(name = "recurrence")
public class RecurrenceBean {
	
	private String rule;
	
	public RecurrenceBean() {

	}
	
	public RecurrenceBean(String rule) {
		this.rule = rule;
	}	
	
	public String getRule() {
		return rule;
	}

	public void setRule(String rule) {
		this.rule = rule;
	}
	
	
}
