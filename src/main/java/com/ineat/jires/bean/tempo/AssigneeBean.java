package com.ineat.jires.bean.tempo;

import javax.xml.bind.annotation.XmlRootElement;

@XmlRootElement(name = "assignee")
public class AssigneeBean {
	
	private String key;
	private String type;
	
	public AssigneeBean() {
		
	};
	
	public String getKey() {
		return key;
	}
	public void setKey(String key) {
		this.key = key;
	}
	public String getType() {
		return type;
	}
	public void setType(String type) {
		this.type = type;
	}

}
