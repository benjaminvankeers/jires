package com.ineat.jires.bean.tempo;

import javax.xml.bind.annotation.XmlRootElement;

@XmlRootElement(name = "scope")
public class ScopeBean {

	private Long id;
	private String type;
	
	public ScopeBean() {

	}
	
	public ScopeBean(String type) {
		this.type = type;
	}

	
	public Long getId() {
		return id;
	}
	public void setId(Long id) {
		this.id = id;
	}
	public String getType() {
		return type;
	}
	public void setType(String type) {
		this.type = type;
	}
	
	
}
