package com.ineat.jires.bean.tempo;

import javax.xml.bind.annotation.XmlRootElement;

@XmlRootElement
public class PlanBean {
	
	  private int commitment;
	  private String start;
	  private String end;
	  private String from;
	  private AssigneeBean assignee;
	  private PlanItemBean planItem;
	  private RecurrenceBean recurrence;
	  private ScopeBean scope;
	  
	public PlanBean() {
		
	}
	  
	public int getCommitment() {
		return commitment;
	}
	public void setCommitment(int commitment) {
		this.commitment = commitment;
	}
	public String getStart() {
		return start;
	}
	public void setStart(String start) {
		this.start = start;
	}
	public String getEnd() {
		return end;
	}
	public void setEnd(String end) {
		this.end = end;
	}
	public AssigneeBean getAssignee() {
		return assignee;
	}
	public void setAssignee(AssigneeBean assignee) {
		this.assignee = assignee;
	}
	public PlanItemBean getPlanItem() {
		return planItem;
	}
	public void setPlanItem(PlanItemBean planItem) {
		this.planItem = planItem;
	}
	public RecurrenceBean getRecurrence() {
		return recurrence;
	}
	public void setRecurrence(RecurrenceBean recurrence) {
		this.recurrence = recurrence;
	}
	public ScopeBean getScope() {
		return scope;
	}
	public void setScope(ScopeBean scope) {
		this.scope = scope;
	}

	public String getFrom() {
		return from;
	}

	public void setFrom(String from) {
		this.from = from;
	}
	  
	  
	  

}
