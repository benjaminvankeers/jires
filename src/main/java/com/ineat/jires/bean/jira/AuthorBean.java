package com.ineat.jires.bean.jira;

import javax.xml.bind.annotation.XmlRootElement;

@XmlRootElement(name = "author")
public class AuthorBean {
	
	private String name;
	
	public AuthorBean() {
		
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}
	
	

}
