package com.ineat.jires.bean.jira;

import javax.xml.bind.annotation.XmlRootElement;

@XmlRootElement
public class WorklogBean {
	
	
	private String comment;
	private String started;
	private int timeSpentSeconds;
	private AuthorBean author;
	
	public WorklogBean() {
		
	}
	
	public String getComment() {
		return comment;
	}
	public void setComment(String comment) {
		this.comment = comment;
	}
	public String getStarted() {
		return started;
	}
	public void setStarted(String started) {
		this.started = started;
	}
	public int getTimeSpentSeconds() {
		return timeSpentSeconds;
	}
	public void setTimeSpentSeconds(int timeSpentSeconds) {
		this.timeSpentSeconds = timeSpentSeconds;
	}
	public AuthorBean getAuthor() {
		return author;
	}
	public void setAuthor(AuthorBean author) {
		this.author = author;
	}	
	
	

}
