package com.ineat.jires;

import com.ineat.jires.bean.tempo.PlanBean;
import com.sun.jersey.api.client.Client;
import com.sun.jersey.api.client.ClientResponse;
import com.sun.jersey.api.client.WebResource;
import com.sun.jersey.api.client.filter.HTTPBasicAuthFilter;

public class TempoClient {
	
	public TempoClient() {
		
	}
	
	public void postPlan(PlanBean plan) {
		Client client = new Client();
		client.addFilter(new HTTPBasicAuthFilter("benjaminvan", "Ineat59"));
 
		WebResource webResource = client.resource("https://dev-jira.ineat-conseil.fr/rest/tempo-planning/1/allocation");

	
		ClientResponse response = webResource.accept("application/json")
                .type("application/json").post(ClientResponse.class, plan);
		
		System.out.println(response.getStatus());
	}
	
	

}
