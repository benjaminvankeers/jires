package com.ineat.jires;

import com.ineat.jires.bean.jira.WorklogBean;
import com.sun.jersey.api.client.Client;
import com.sun.jersey.api.client.ClientResponse;
import com.sun.jersey.api.client.WebResource;
import com.sun.jersey.api.client.filter.HTTPBasicAuthFilter;

public class JiraClient {
	
	public JiraClient() {
		
	}
	
	public void postWorkLog(WorklogBean worklog, String tache) {
		Client client = new Client();
		client.addFilter(new HTTPBasicAuthFilter("benjaminvan", "Ineat59"));
 
		WebResource webResource = client.resource("https://dev-jira.ineat-conseil.fr/rest/api/2/issue/"+tache+"/worklog");

	
		ClientResponse response = webResource.accept("application/json")
                .type("application/json").post(ClientResponse.class, worklog);
		
		System.out.println(response.getStatus());
	}

}
